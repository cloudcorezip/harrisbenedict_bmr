choose foods / drinks based on calories you need per day using harris-benedict calculation with activity factor
MEN BMR   = (10 * weight) + (6.25 * height) - (5 * age) + 5
WOMEN BMR = (10 * weight) + (6.25 * height) - (5 * age) - 161

after we get the BMR we multipylied the BMR with the activty factor
Light     (1-3 days/week)    = BMR * 1.375
Moderate  (3-5 days/week)    = BMR * 1.55
Active    (6-7 days/week)    = BMR * 1.725
Sedentary (0 days/week)      = BMR * 1.2

2200 

we divide the foods by 4 (Breakfast, Lunch, dinner, snacks)
breakfast at 7am-8am
lunch at 12.30pm-1pm
dinner at 6pm-6.30pm
snacks 10am-11am & 2pm-3pm

for breakast usually take the least caloris out of 3 big meals (breakfast, lunch, dinner), while snacks or actually can do it without even a snacks 

so basicall if your BMR is 2200 and you do a little exercise or no exercise at all the schedule looks like
2640
300 cal on breakfast
500 cal on lunch
500 cal on dinner
400 cal on snacks



## References
https://www.goodto.com/wellbeing/best-time-to-eat-breakfast-lunch-dinner-115224 
https://www.omnicalculator.com/health/bmr-harris-benedict-equation#what-is-a-bmr-calculator