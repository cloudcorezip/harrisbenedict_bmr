import enum
import asyncio
import random
import numpy as np
import pickle
import json
import re
from flask import Flask, render_template, request, jsonify
from nltk import data
from fatsecret import Fatsecret
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from tensorflow.keras.models import load_model
from nltk.stem import WordNetLemmatizer
#from flask_ngrok import run_with_ngrok
import nltk
lemmatizer = WordNetLemmatizer()
factory = StemmerFactory()
stemmer = factory.create_stemmer()  

model = load_model("model.h5")
intents = json.loads(open("intents.json").read())
foods_menu = json.loads(open("menu.json").read())
words = pickle.load(open("words.pkl", "rb"))
classes = pickle.load(open("classes.pkl", "rb"))

app = Flask(__name__)

client_secret = 'ade42bd5d772407a977cb429d4418e40'
client_key = '90cab7b44e8a412d955ea055446f221f'

datadict = {
	"berat": None,
	"tinggi": None,
	"umur": None,
	"gender": None
}

regexdict = {
	"berat_regex": "\d+ kg|\d+kg",
	"tinggi_regex": "\d+ cm|\d+cm",
	"umur_regex": "\d+thn|\d+ tahun|\d+tahun|\d+ th|\d+th",
	"gender_regex": "pria|wanita"
}

def json_response(answer, misc=''):
	return jsonify(
		answer=answer,
		tag=misc
	)

# app.run(debug=True)
@app.route("/")
def home():
	return render_template('index.html')

@app.route("/answer", methods=["POST"])
def check_answer():
	exclude=['gender']
	temp_dict = {k:v for k,v in datadict.items() if k not in exclude}
	if all (temp_dict.values()):
		if datadict.get('gender') == None:
			return json_response(
				'Step terakhir, masukkan gender(pria/wanita)',
				'input_validation'
			)
		else:
			bmr = calculate_calories()
			return food_suggestion(bmr)
	else:
		return json_response(temp_dict)

@app.route("/get", methods=["POST"])
def chatbot_response():
	msg = request.form["msg"]
	ints = predict_class(msg, model)
	bot_response = getResponse(ints, intents)
	print(ints)

	if 'dan' in ints[0]['intent'] or 'all' in ints[0]['intent']:
		keys = ints[0]['intent'].replace(' ', '')[4:].split('dan') if 'dan' in ints[0]['intent'] else ['berat', 'tinggi', 'umur']
		for i in keys:
			regex = regexdict.get(f"{i}_regex")
			value = re.search(regex, msg)
			if value == None:
				return json_response(
					"pastikan satuan / data terisi dengan benar",
					'salah_data'
				)
			else:
				datadict[i] = value.group(0)
	elif 'reset' in ints[0]['intent']:
		resetData()
	elif 'data' in ints[0]['intent']:
		key = ints[0]['intent'][5:]
		regex = regexdict.get(f"{key}_regex")
		value = re.search(regex, msg.lower())
		if value == None:
			return json_response(
				"pastikan satuan / data terisi dengan benar",
				'salah_data'
			)
		else:
			datadict[key] = value.group(0)
	elif msg.split()[0] == '!infogizi':
		nutrition = nutrition_facts(msg.split()[1])
		return json_response(
			nutrition,
			'info_gizi'
		)

	return json_response(
		bot_response,
		ints[0]['intent']
	)

def food_suggestion(bmr):

	lunch			= []
	dinners		= []

	breakfast_calories = bmr / 3 - 50
	lunch_calories = bmr / 3 + 100
	dinners_calories = bmr / 3 - 50		


	#get the bmr
	# randomize foods based on bmr total 
	# divide by 3 breakfast, lunch, dinners
	# snacks (150 calories)
	# 
	breakfast = set_breakfast(breakfast_calories)
	lunch = set_lunch(lunch_calories)
	dinners = set_dinners(dinners_calories)

	temp = {
		'bmr': bmr,
		'sum_breakfast': breakfast_calories,
		'sum_lunch': lunch_calories,
		'sum_dinners': dinners_calories,
		'breakfast': breakfast,
		'lunches': lunch,
		'dinners': dinners,
		'snacks': random.choice(foods_menu['snacks']),
		'buah': random.sample(foods_menu['buah'], 3)
	}

	print(temp)

	resetData()
	return json_response(
		temp, 
		"food_suggestion"
	)

def nutrition_facts(name):
	fs = fs = Fatsecret(client_key, client_secret)

	food_id = fs.foods_search(name)[0]['food_id']
	return fs.food_get(food_id)

def set_breakfast(max_cal):
	breakfast = []
	cal_total_temp = 0

	while cal_total_temp < max_cal: 
		if(len(breakfast) == 0): 
			temp = random.choice([x for x in foods_menu['sarapan'] if x["food_type"] == "carbs" or x["food_type"] == "misc" or x["food_type"] == "heavy_misc"])
			cal_total_temp += temp['kalori_per_100']
			breakfast.append(temp)
		elif any(d['food_type'] == 'heavy_misc' for d in breakfast):
			break
		elif any(d['food_type'] == 'misc' for d in breakfast):
			temp = random.choice([x for x in foods_menu['sarapan'] if x["food_type"] == "side_dish"])
			if any(d['nama'] == temp['nama'] for d in breakfast):
					continue
			cal_total_temp += temp['kalori_per_100']
			breakfast.append(temp)
		elif any(d['food_type'] == 'carbs' for d in breakfast):
			temp = random.choice([x for x in foods_menu['sarapan'] if x["food_type"] == "side_dish" or x["food_type"] == "vegs"])
			cal_total_temp += temp["kalori_per_porsi"] if temp["food_type"] == "vegs" else temp['kalori_per_100']
			breakfast.append(temp)
	
	return breakfast

def set_lunch(max_cal):

	lunches = []
	cal_total_temp = 0

	while cal_total_temp < max_cal: 
		if(len(lunches) == 0): 
			temp = random.choice([x for x in foods_menu['lunches'] if x["food_type"] == "carbs"])
			cal_total_temp += temp['kalori_per_100']
			lunches.append(temp)
		elif any(d['food_type'] == 'misc' for d in lunches):
			temp = random.choice([x for x in foods_menu['lunches'] if x["food_type"] == "side_dish"])
			if any(d['nama'] == temp['nama'] for d in lunches):
					continue
			cal_total_temp += temp['kalori_per_100']
			lunches.append(temp)
		elif any(d['food_type'] == 'carbs' for d in lunches):
			temp = random.choice([x for x in foods_menu['lunches'] if x["food_type"] == "side_dish" or x["food_type"] == "vegs"])
			cal_total_temp += temp['kalori_per_porsi'] if temp["kalori_per_100"] == None else temp['kalori_per_100']
			lunches.append(temp)

	return lunches

def set_dinners(max_cal):
	dinners = []
	cal_total_temp = 0

	while cal_total_temp < max_cal: 
		if(len(dinners) == 0): 
			temp = random.choice([x for x in foods_menu['dinners'] if x["food_type"] == "carbs" or x["food_type"] == "misc"])
			cal_total_temp += temp['kalori_per_100']
			dinners.append(temp)
		elif any(d['food_type'] == 'misc' for d in dinners):
			break
		elif any(d['food_type'] == 'carbs' for d in dinners):
			temp = random.choice([x for x in foods_menu['dinners'] if x["food_type"] == "side_dish" or x["food_type"] == "vegs"])
			if any(d['nama'] == temp['nama'] for d in dinners):
				continue
			cal_total_temp += temp['kalori_per_100']
			dinners.append(temp)

	return dinners

def clean_up_sentence(sentence):
	sentence_words = nltk.word_tokenize(sentence)
	sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
	return sentence_words

def bow(sentence, words, show_details=True):
	sentence_words = clean_up_sentence(sentence)
	bag = [0] * len(words)
	for s in sentence_words:
			for i, w in enumerate(words):
					if w == s:
							bag[i] = 1
							if show_details:
									print("found: %s" % w)
	return np.array(bag)

def predict_class(sentence, model):
	p = bow(sentence, words, show_details=False)
	res = model.predict(np.array([p]))[0]
	ERROR_THRESHOLD = 0.25
	# if prob > than 25% return
	results = [[i, r] for i, r in enumerate(res) if r > ERROR_THRESHOLD]
	results.sort(key=lambda x: x[1], reverse=True)
	return_list = []
	for r in results:
			return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
	return return_list	

def getResponse(ints, intents_json):
	tag = ints[0]["intent"]
	list_of_intents = intents_json["intents"]
	for i in list_of_intents:
			if i["tag"] == tag:
					result = random.choice(i["responses"])
					break
	return result

def resetData():
		for keys in datadict:
			datadict[keys] = None

## calculate the Calories
def calculate_calories():
	exclude=['gender']
	temp_dict = {k:v for k,v in datadict.items() if k not in exclude}

	temp = []
	for key, value in temp_dict.items():
		temp.append(int(re.search("\d+", value).group(0)))

	bmr = 0
	gender = datadict.get('gender')
	if gender == 'pria':
		bmr = 10 * temp[0] + 6.25*temp[1] - 5 * temp[2] + 5
	else:
		bmr = 10 * temp[0] + 6.25*temp[1] - 5 * temp[2] - 161

	return bmr

def get_numbers(dict_keys):
	return datadict.get(dict_keys)



if __name__ == "__main__":
	app.run(debug=True)
